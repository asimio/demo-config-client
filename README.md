# README #

Demo Config client accompanying source code for blog post at [http://tech.asimio.net/2016/12/09/Centralized-and-Versioned-Configuration-using-Spring-Cloud-Config-Server-and-Git.html](http://tech.asimio.net/2016/12/09/Centralized-and-Versioned-Configuration-using-Spring-Cloud-Config-Server-and-Git.html)

### Requirements ###

* Java 8
* Maven 3.3.x
* A [Eureka server](https://bitbucket.org/asimio/discoveryserver) instance for this Demo Config client and the Config server in a registration-first approach to register with.
* A [Config server](https://bitbucket.org/asimio/configserver) instance for this Demo Config client to read remote properties from.

### Building the artifact ###

```
mvn clean package
```

### Running the application in a config-first approach ###

```
java -Dspring.profiles.active=known-config-server,development -Dspring.cloud.config.uri=http://localhost:8101 -DhostName=localhost -jar target/demo-config-client.jar
```
where a [Config server](https://bitbucket.org/asimio/configserver) instance is running at http://localhost:8101 in a *config-first* approach and a [Discovery server](https://bitbucket.org/asimio/discoveryserver) instance is available at http://localhost:8001/eureka

### Running the application in a registration-first approach ###

```
java -Dspring.profiles.active=registered-config-server,development -DhostName=localhost -Deureka.client.serviceUrl.defaultZone=http://localhost:8001/eureka/ -jar target/demo-config-client.jar
```
where a [Config server](https://bitbucket.org/asimio/configserver) instance is running at http://localhost:8101 in a *registration-first* approach and a [Discovery server](https://bitbucket.org/asimio/discoveryserver) instance is available at http://localhost:8001/eureka

### Available endpoints ###

```
curl -v "http://localhost:8700/actors/1"
```

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero